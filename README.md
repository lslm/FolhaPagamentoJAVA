# Folha de Pagamento em Java

Uma empresa deseja gerenciar a folha de pagamento dos seus funcionários.

Em uma aplicação de folha de pagamento anterior, você implementou 3 classes:
 * Empresa (cnpj, razaoSocial, nomeFantasia)
 * Departamento (nome)
 * Funcionario (cpf, nome, salario)

O salário do funcionário era armazenado em um atributo da classe Funcionario. No entanto, nesta nova aplicação, o salário do funcionário deverá ser calculado pela aplicação. Serão dois tipos de funcionários: **horistas** e **mensalistas**.

Os horistas trabalham uma determinada quantidade de horas por mês e recebem um valor X por hora. O salário deles é igual a **horasTrabalhadas * valorHora**. Os mensalistas têm um cargo (ex: analista, programador etc.) e, dependendo do cargo, ele recebe um salário específico (ex: programadores recebem 5000, analistas recebem 6000) etc.

Escreva o código da aplicação nova (e da antiga), assumindo as seguintes classes:
 * Empresa (cnpj, razaoSocial, nomeFantasia) - métodos: calcularSalarioMedio
 * Departamento (nome) - métodos: calcularSalarioMedio
 * Funcionario (cpf, nome, salario)
 * FuncionarioHorista (horasTrabalhas, valorHora) herda de Funcionario - métodos: getSalario sobrescrito
 * FuncionarioMensalista (cargo) herda de funcionario - métodos: getSalario sobrescrito


### É isso aí, moçada!

*Descrição do problema escrita pelo professor Leandro Luque, disponível [aqui] (https://poofatec.wordpress.com/)*
