package folhapagamento;

import java.util.ArrayList;

public class Departamento {
    private String nome;
    ArrayList<Funcionario> funcionarios = new ArrayList();
    
    public Departamento(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
    
    public void AdicionarFuncionario(Funcionario funcionario) {
        this.funcionarios.add(funcionario);
    }
}
