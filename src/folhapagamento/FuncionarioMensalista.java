package folhapagamento;

public class FuncionarioMensalista extends Funcionario {
    private Cargos cargo;
    
    public FuncionarioMensalista(Cargos cargo, String nome, String cpf, double salario) {
        this.cargo = cargo;   
        this.setCpf(cpf);
        this.setNome(nome);
        this.setSalario(salario);
    }

    public Cargos getCargo() {
        return cargo;
    }
    
    @Override
    public double getSalario() {
        if (this.cargo == Cargos.PROGRAMADOR) {
            return 2000.00;
        } else if (this.cargo == Cargos.ANALISTA) {
            return 4000.00;
        } else {
            return 6000.00;
        }
    }
}
